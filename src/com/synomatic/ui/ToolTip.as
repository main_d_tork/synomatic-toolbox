/**
 Copyright (c) 2012 syn-o-matic inc. 
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 * */


package com.synomatic.ui
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	import mx.charts.chartClasses.NumericAxis;
	
	public class ToolTip extends MovieClip
	{
		
		private var arrowCounter:uint = 0;
		private var pointer:DisplayObjectContainer;	
		private var txtField:TextField;
		private var _target:Object;
		private var _color:uint = 0;
		private var _removable:Boolean = true;
		
		public function ToolTip()
		{
			super();
			
			initialize();
		}
		
		/* Overriden Methods: ------------------------------------------------ *
		* ------------------------------------------------------------------- */
		
		/* Public Methods: --------------------------------------------------- *
		* ------------------------------------------------------------------- */
		
		/* Getter/Setters: --------------------------------------------------- *
		* ------------------------------------------------------------------- */
		public function get text():String
		{
			return txtField.text;
		}
		
		public function set text(value:String):void
		{
				txtField.text = value;	
		}
		
		
		public function set target(value:Object):void
		{
			_target = value;	
			
			if (this.contains(pointer) && _removable) 
			{
				
				target.addEventListener(MouseEvent.MOUSE_OVER, onTargetMouseOver);
			}
		}
		
		
		public function get target():Object
		{
			return _target;
		}
		
		
		public function get color():uint
		{
			return _color;
		}
		
		public function set color(value:uint):void
		{
			_color = value;
			
			var tf:TextFormat = txtField.defaultTextFormat;
			tf.color = _color;
			txtField.setTextFormat(tf);
			
			
		}
		
		
		public function set removable(value:Boolean):void
		{
			_removable = value;
		}
		
		
		public function get removable():Boolean
		{
			return _removable;
		}
		
		/* Semi-Public Methods:	---------------------------------------------- *
		* ------------------------------------------------------------------- */
		
		/* Protected Methods: ------------------------------------------------ *
		* ------------------------------------------------------------------- */		 
		
		/* Private Methods: -------------------------------------------------- *
		* ------------------------------------------------------------------- */
		
		private function initialize():void
		{
			
			pointer = new Arrow();	
			addChild(pointer);
			pointer.scaleX = pointer.scaleY = 1.2;
			
			pointer.addEventListener(Event.ENTER_FRAME, onArrowHover);	
			
			txtField = new TextField();
			txtField.width = 100;
			txtField.height = 20;
			txtField.autoSize = TextFieldAutoSize.LEFT;
			txtField.multiline = true;
			txtField.selectable = false;
			txtField.alpha = 0.9;
			txtField.embedFonts = true;
			/*
			var tF:TextFormat = StyleFormatter.GetRegularTextFormat();
			tF.color = 0xffffff;
			txtField.defaultTextFormat = tF;
			*/
			
			var tFormat:TextFormat = new TextFormat();
			with (tFormat) 
			{
				font = "hooge 05_55 Cyr2";
				bold = false;
				size = 10;
				color = _color;
			}
			txtField.defaultTextFormat = tFormat;
			txtField.x = 15;
			txtField.y = 5;
			pointer.addChild(txtField);
		}
		
		
		
		/* Event Handlers: --------------------------------------------------- *
		* ------------------------------------------------------------------- */

		
		private function onArrowHover(event:Event):void{
			
			/*var x0:int = 0;
			var y0:int = 0;
			
			trace(target.width);
			if (target != null)
			{
				x0 = target.width;
				y0 = target.height;
			}	
			*/
			
			var opVal:Number = arrowCounter/7;			
			var A:uint = 7;
			var dY:Number = A*Math.sin(45 + opVal);
			var dX:Number = A*(1-Math.pow(Math.sin(45 + opVal),2));
			
			var d0:Number = Math.sin(opVal);
			
			with (event.target)
			{
				y =  A*d0;
				x =  A*d0;
				scaleX = scaleY = 0.98 + d0*0.1;
				
				//rotation = Math.atan2(dY,dX)*180/(4*Math.PI) - 35;
			}			
			
			if (opVal >= 360)
				arrowCounter = 0;
			arrowCounter++;
		}
		
		private function BlurTheArrow(event:Event):void{
			with (event.target)
			{
				y++;
				x++;
				alpha -= 0.05;
				if (alpha <= 0)
				{
					pointer.removeEventListener(Event.ENTER_FRAME, BlurTheArrow);
					pointer.removeEventListener(Event.ENTER_FRAME, onArrowHover);
					//this.removeChild(DisplayObject(event.target));
					parent.parent.removeChild(this);
				}
			}
		}
		
		
		private function onTargetMouseOver(event:MouseEvent):void
		{
			
			if (!_removable)
				return;
			
			pointer.removeEventListener(Event.ENTER_FRAME, onArrowHover);
			target.removeEventListener(MouseEvent.MOUSE_OVER, onTargetMouseOver);
			
			if (!pointer.hasEventListener(Event.ENTER_FRAME))
			{
				pointer.addEventListener(Event.ENTER_FRAME, BlurTheArrow);
			}
		}

	}
}