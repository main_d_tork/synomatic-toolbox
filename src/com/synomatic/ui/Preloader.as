package com.synomatic.ui
{
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	import mx.events.EffectEvent;
	import mx.events.FlexEvent;
	import mx.events.RSLEvent;
	import mx.graphics.RectangularDropShadow;
	import mx.preloaders.IPreloaderDisplay;
	
	import spark.effects.Fade;
	
	public class Preloader extends Sprite implements IPreloaderDisplay
	{
		
		private const DELAY_AFTER_LOAD:uint = 0;
		
		private var sequence:PreloaderSequence;
		private var progressText:TextField;
		
		private var rslDownloading:Boolean;
		private var rslCompleted:Number;     
		private var rslTotal:Number;
		
		//--------------------------------------------------------------------------
		//
		//  Variables
		//
		//--------------------------------------------------------------------------
		
		private var _barWidth:Number;
		private var _bgSprite:Sprite;
		private var _barSprite:Sprite;
		private var _barFrameSprite:Sprite;
		private var _downloadComplete:Boolean = false;
		private var _showingDisplay:Boolean = false;
		private var _initProgressCount:uint = 0;
		private var _startTime: uint;
		
		protected var initProgressTotal:uint = 6;

		
		public function Preloader()
		{
			super();
		}
		
		
		public function get backgroundAlpha():Number
		{
				return 1;
		}
		
		public function set backgroundAlpha(value:Number):void
		{
		}
		
		public function get backgroundColor():uint
		{
			return 0xFFFFFF;
		}
		
		
		public function get backgroundImage():Object
		{
			return null
		}
		
		public function set backgroundImage(value:Object):void
		{
		}
		
		public function set backgroundColor(value:uint):void
		{
		}
		
		public function get backgroundSize():String
		{
			return null;
		}
		
		public function set backgroundSize(value:String):void
		{
		}
		
		
		/**
		 *  @private
		 *  Storage for the stageHeight property.
		 */
		private var _stageHeight:Number = 375;
		
		/**
		 *  The height of the stage,
		 *  which is passed in by the Preloader class.
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 10
		 *  @playerversion AIR 1.5
		 *  @productversion Flex 4
		 */
		public function get stageHeight():Number 
		{
			return _stageHeight;
		}
		
		/**
		 *  @private
		 */
		public function set stageHeight(value:Number):void 
		{
			_stageHeight = value;
		}
		
		//----------------------------------
		//  stageWidth
		//----------------------------------
		
		/**
		 *  @private
		 *  Storage for the stageHeight property.
		 */
		private var _stageWidth:Number = 500;
		
		/**
		 *  The width of the stage,
		 *  which is passed in by the Preloader class.
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 10
		 *  @playerversion AIR 1.5
		 *  @productversion Flex 4
		 */
		public function get stageWidth():Number 
		{
			return _stageWidth;
		}
		
		/**
		 *  @private
		 */
		public function set stageWidth(value:Number):void 
		{
			_stageWidth = value;
		}
		
		
		private var _preloader:Sprite; 
		
		/**
		 *  The Preloader class passes in a reference to itself to the display class
		 *  so that it can listen for events from the preloader.
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 10
		 *  @playerversion AIR 1.5
		 *  @productversion Flex 4
		 */
		public function set preloader(value:Sprite):void
		{
			_preloader = value;
			
			value.addEventListener(ProgressEvent.PROGRESS, progressHandler);    
			value.addEventListener(Event.COMPLETE, completeHandler);
			
			value.addEventListener(RSLEvent.RSL_PROGRESS, rslProgressHandler);
			value.addEventListener(RSLEvent.RSL_COMPLETE, rslCompleteHandler);
			value.addEventListener(RSLEvent.RSL_ERROR, rslErrorHandler);
			
			value.addEventListener(FlexEvent.INIT_PROGRESS, initProgressHandler);
			value.addEventListener(FlexEvent.INIT_COMPLETE, initCompleteHandler);
		}
		
		
		protected function completeHandler(event:Event):void
		{
		}
		
		protected function rslCompleteHandler(event:RSLEvent):void
		{
		}
		
		protected function rslErrorHandler(event:RSLEvent):void
		{
			_preloader.removeEventListener(ProgressEvent.PROGRESS, progressHandler);    
			_preloader.removeEventListener(Event.COMPLETE, completeHandler);
			_preloader.removeEventListener(RSLEvent.RSL_PROGRESS, rslProgressHandler);
			_preloader.removeEventListener(RSLEvent.RSL_COMPLETE, rslCompleteHandler);
			_preloader.removeEventListener(RSLEvent.RSL_ERROR, rslErrorHandler);
			_preloader.removeEventListener(FlexEvent.INIT_PROGRESS, initProgressHandler);
			_preloader.removeEventListener(FlexEvent.INIT_COMPLETE, initCompleteHandler);
			
			if (!_showingDisplay)
			{
				show();
				// Call setDownloadProgress here to draw
				// the progress bar background.
				setDownloadProgress(100, 100);
			}
			
			var errorField:ErrorField = new ErrorField(this);
			errorField.show(event.errorText);
		}
		
		protected function initProgressHandler(event:Event):void
		{
			_initProgressCount++;
			
			if (!_showingDisplay)
			{
				show();
				
				// If we are showing the progress for the first
				// time here, we need to call setDownloadProgress() once to draw
				// the progress bar background.
				setDownloadProgress(100, 100);
			}
			
			if (_showingDisplay)
			{
				// if show() did not actually show because of SWFObject bug
				// then we may need to draw the download bar background here
				if (!_downloadComplete)
					setDownloadProgress(100, 100);
				
				setInitProgress(_initProgressCount, initProgressTotal);
			}
		}
		
		
		
		public function initialize():void
		{
			
			_startTime = getTimer();
			
			var g:Graphics = graphics;
			
			
			
			// Determine the size
			var totalWidth:Number = Math.min(stageWidth - 10, 207);
			var totalHeight:Number = 19;
			var startX:Number = Math.round((stageWidth - totalWidth) / 2);
			var startY:Number = Math.round((stageHeight - totalHeight) / 2);
			
			_barWidth = totalWidth - 10;
			
			_bgSprite = new Sprite();
			_barFrameSprite = new Sprite();
			_barSprite = new Sprite();
			
			addChild(_bgSprite);
			addChild(_barFrameSprite);  
			addChild(_barSprite);
			
			_barFrameSprite.x = _barSprite.x = startX + 5;
			_barFrameSprite.y = _barSprite.y = startY + 5;
			
			// Draw the background/shadow
			g = _bgSprite.graphics;
			g.lineStyle(1, 0x636363);
			g.beginFill(0xE8E8E8);
			g.drawRect(startX, startY, totalWidth, totalHeight);
			g.endFill();
			g.lineStyle();
			
			g = graphics;
			var ds:RectangularDropShadow = new RectangularDropShadow();
			ds.color = 0x000000;
			ds.angle = 90;
			ds.alpha = .6;
			ds.distance = 2;
			ds.drawShadow(g, 
				startX,
				startY,
				totalWidth,
				totalHeight);
			
			/*var chromeColor:uint = getPreloaderChromeColor();
			
			if (chromeColor != DEFAULT_COLOR)
			{
				var colorTransform:ColorTransform = new ColorTransform();
				
				colorTransform.redOffset = ((chromeColor & (0xFF << 16)) >> 16) - DEFAULT_COLOR_VALUE;
				colorTransform.greenOffset = ((chromeColor & (0xFF << 8)) >> 8) - DEFAULT_COLOR_VALUE;
				colorTransform.blueOffset = (chromeColor & 0xFF) - DEFAULT_COLOR_VALUE;
				
				_bgSprite.transform.colorTransform = colorTransform;
				_barFrameSprite.transform.colorTransform = colorTransform;
				_barSprite.transform.colorTransform = colorTransform;
			}*/
			
			
			sequence = new PreloaderSequence();
			addChild(sequence);
			var myDropShadowFilter:DropShadowFilter = new DropShadowFilter(10, 45, 
				0x333333, 0.8, 8, 8, 0.65, BitmapFilterQuality.MEDIUM, false, false);
			sequence.filters = [myDropShadowFilter];

			alpha = 0.9;
			sequence.x = startX + 30;
			sequence.y = startY - 100;
			
			
			var fontIns:TFont;
			var format:TextFormat	    = new TextFormat();
			format.font		     	 	= "hoogeNoCFF";
			format.bold					= false;
			format.color                = 0x000000;
			format.size                 = 8;
			format.align				= TextFormatAlign.CENTER;
			
			progressText 					= new TextField(); 
			progressText.alpha				= 0.5;
			progressText.x					= startX + 50;    
			progressText.y					= startY + 20;
			progressText.height 			= 400;
			progressText.autoSize			= TextFieldAutoSize.CENTER;
			progressText.antiAliasType      = AntiAliasType.NORMAL;
			progressText.embedFonts 		= true;
			progressText.defaultTextFormat 	= format;
			addChild(progressText);
			
			
		
		}
		
		
		/*private static const DEFAULT_COLOR:uint = 0xCCCCCC;
		private static const DEFAULT_COLOR_VALUE:uint = 0xCC;*/
		
		/*private function getPreloaderChromeColor():uint
		{
			const sm:ISystemManager = parent.parent as ISystemManager;
			const valueObject:Object = (sm) ? sm.info()["preloaderChromeColor"] : null;
			
			var valueString:String = valueObject as String;
			if (valueString && (valueString.charAt(0) == "#"))
				valueString = "0x" + valueString.substring(1);
			
			return (valueString) ? uint(valueString) : DEFAULT_COLOR;
		}*/
		
		
		
		private var lastBarWidth:Number = 0;
		
	    protected function setDownloadProgress(completed:Number, total:Number):void
		{
			/*if (!_barFrameSprite)
				return;*/
			
			const outerHighlightColors:Array = [0xFFFFFF, 0xFFFFFF];
			const outerHighlightAlphas:Array = [0.12, 0.80];
			const fillColors:Array = [0xA9A9A9, 0xBDBDBD];
			const fillAlphas:Array = [1, 1];
			const ratios:Array = [0, 255];
			
			var w:Number = Math.round(_barWidth * Math.min(completed / total, 1));
			var h:Number = 9;
			var g:Graphics = _barFrameSprite.graphics;
			var m:Matrix = new Matrix();
			
			// Make sure the download progress bar never regresses in size.
			w = Math.max(w, lastBarWidth);
			lastBarWidth = w;
			
			m.createGradientBox(w, h, 90);
			
			g.clear();
			
			// Outer highlight
			g.lineStyle(1);
			g.lineGradientStyle("linear", outerHighlightColors, outerHighlightAlphas, ratios, m);
			g.drawRect(0, 0, w, h);
			
			// border/fill
			g.lineStyle(1, 0x636363);
			g.beginGradientFill("linear", fillColors, fillAlphas, ratios, m);
			g.drawRect(1, 1, w - 2, h - 2);
			g.endFill();
			
			// highlight
			g.lineStyle(1, 0, 0.12);
			g.moveTo(2, h - 1);
			g.lineTo(2, 2);
			g.lineTo(w - 2, 2);
			g.lineTo(w - 2, h - 1);
			
			if (completed == total)
				_downloadComplete = true
		}
		
		
		protected function setInitProgress(completed:Number, total:Number):void
		{
			const highlightColors:Array = [0xFFFFFF, 0xEAEAEA];
			const fillColors:Array = [0xFFFFFF, 0xD8D8D8];
			const alphas:Array = [1, 1];
			const ratios:Array = [0, 255];
			
			var w:Number = Math.round(_barWidth * Math.min(completed / total, 1));
			var h:Number = 9;
			var g:Graphics = _barSprite.graphics;
			var m:Matrix = new Matrix();
			
			m.createGradientBox(w - 6, h - 2, 90, 2, 2);
			
			g.clear();
			
			// highlight/fill
			g.lineStyle(1);
			g.lineGradientStyle("linear", highlightColors, alphas, ratios, m);
			g.beginGradientFill("linear", fillColors, alphas, ratios, m);
			g.drawRect(2, 2, w - 4, h - 4);
			g.endFill();
			
			// divider line
			g.lineStyle(1, 0, 0.55);
			g.moveTo(w - 1, 2);
			g.lineTo(w - 1, h - 1);
		}
		
		protected function progressHandler(event:ProgressEvent):void
		{
			var loaded:uint = event.bytesLoaded;
			var total:uint = event.bytesTotal;
			
			if( rslDownloading ) 
			{
				loaded -= rslCompleted;
				total -= rslTotal;
			}
			
			progressText.text = Number(loaded/1024).toFixed(1) + "  kB  loaded  out   of  "  + Number(total/1024).toFixed(1);
			
			trace("[preloader]: " + Number(loaded/1024).toFixed(1) + "  kB  loaded  out   of  "  + Number(total/1024).toFixed(1));
			
			
			// Only show the Loading phase if it will appear for awhile.
			if (!_showingDisplay)
				show();
			
			if (_showingDisplay)        
				setDownloadProgress(loaded, total);
		}
		
		
		private function show():void
		{
			// swfobject reports 0 sometimes at startup
			// if we get zero, wait and try on next attempt
			if (stageWidth == 0 && stageHeight == 0)
			{
				try
				{
					stageWidth = stage.stageWidth;
					stageHeight = stage.stageHeight
				}
				catch (e:Error)
				{
					stageWidth = loaderInfo.width;
					stageHeight = loaderInfo.height;
				}
				if (stageWidth == 0 && stageHeight == 0)
					return;
			}
			
			_showingDisplay = true;
		}
		
		
		
		
		protected function initCompleteHandler(event:Event):void
		{
			var timer:Timer = new Timer(DELAY_AFTER_LOAD,1);
			timer.addEventListener(TimerEvent.TIMER, dispatchComplete);
			timer.start();   
			
			trace("[preloader] loaded for " + ((getTimer() - _startTime)/1000).toString() + " sec");
		}
		
		
		
		
		protected function rslProgressHandler(event:RSLEvent):void
		{
			
			rslDownloading = true;	
			rslCompleted   = event.bytesLoaded;        
			rslTotal       = event.bytesTotal;
			//	_rslFraction    = _rslBytes / _rslTotal;
			
			progressText.text = String(Number(rslCompleted/1024).toFixed(1)) + "  RSLs  loaded  out  of  " + String(Number(rslTotal/1024).toFixed(1));
			
			// Only show the Loading phase if it will appear for awhile.
			if (!_showingDisplay)
				show();
			
			if (_showingDisplay)        
				setDownloadProgress(rslCompleted, rslTotal);
		}

		/*override protected function setDownloadProgress(completed:Number, total:Number):void
		{
			super.setDownloadProgress(completed, total);
		}*/
		
		private function dispatchComplete(event:TimerEvent):void {
			
			var fadeAni:Fade = new Fade(this);
			fadeAni.alphaTo = 0.25;
			fadeAni.duration = 100;
			fadeAni.addEventListener(EffectEvent.EFFECT_END,effectEnd);
			fadeAni.play();
			
			/*	var blurF:Blur = new Blur(this);
			blurF.blurXTo = 2;
			blurF.duration = 100;
			blurF.addEventListener(EffectEvent.EFFECT_END,effectEnd);
			blurF.play();*/
			
		}
		
		
		private function effectEnd(e:EffectEvent): void
		{
			dispatchEvent(new Event(Event.COMPLETE));
			this.alpha = 1;
		}
	}
	
}



import com.synomatic.ui.Preloader;

import flash.display.Sprite;
import flash.system.Capabilities;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

/**
 * @private
 * 
 * Area to display error messages to help debug startup problems.
 * 
 */
class ErrorField extends Sprite
{
	private var downloadProgressBar:Preloader;
	private const MIN_WIDTH_INCHES:int = 2;                    // min width of error message in inches
	private const MAX_WIDTH_INCHES:int = 6;                    // max width of error message in inches
	private const TEXT_MARGIN_PX:int = 10;
	
	
	//----------------------------------
	//  labelFormat
	//----------------------------------
	
	/**
	 *  The TextFormat object of the TextField component of the label.
	 *  This is a read-only property which you must override
	 *  if you need to change it.
	 *  
	 *  @langversion 3.0
	 *  @playerversion Flash 9
	 *  @playerversion AIR 1.1
	 *  @productversion Flex 3
	 */
	protected function get labelFormat():TextFormat
	{
		var tf:TextFormat = new TextFormat();
		tf.color = 0x000000;
		
		tf.font = "Arial";
		tf.size = 12;
		return tf;
	}
	
	
	/**
	 * @private
	 * 
	 * @param - parent - parent of the error field.
	 */ 
	public function ErrorField(downloadProgressBar:Preloader):void
	{
		super();
		this.downloadProgressBar = downloadProgressBar;
	}
	
	
	/**
	 * Create and show the error message.
	 * 
	 * @param errorText - text for error message.
	 *  
	 *  @langversion 3.0
	 *  @playerversion Flash 9
	 *  @playerversion AIR 1.1
	 *  @productversion Flex 3
	 */
	public function show(errorText:String):void
	{
		if (errorText == null || errorText.length == 0)
			return;
		
		var screenWidth:Number = downloadProgressBar.stageWidth;
		var screenHeight:Number = downloadProgressBar.stageHeight;
		
		// create the text field for the message and 
		// add it to the parent.
		var textField:TextField = new TextField();
		
		textField.autoSize = TextFieldAutoSize.LEFT;
		textField.multiline = true;
		textField.wordWrap = true;
		textField.background = true;
		textField.defaultTextFormat = labelFormat;
		textField.text = errorText;
		
		textField.width = Math.max(MIN_WIDTH_INCHES * Capabilities.screenDPI, screenWidth - (TEXT_MARGIN_PX * 2));
		textField.width = Math.min(MAX_WIDTH_INCHES * Capabilities.screenDPI, textField.width);
		textField.y = Math.max(0, screenHeight - TEXT_MARGIN_PX - textField.height);
		
		// center field horizontally
		textField.x = (screenWidth - textField.width) / 2;
		
		downloadProgressBar.parent.addChild(this);
		this.addChild(textField);
		
	}
}