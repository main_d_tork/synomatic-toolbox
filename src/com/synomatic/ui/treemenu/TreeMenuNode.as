﻿/**
 Copyright (c) 2012 syn-o-matic inc. 
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met: 
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 * */
package com.synomatic.ui.treemenu
{
	
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.*;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.text.*;
	import flash.xml.*;
	
	import flashx.textLayout.conversion.TextConverter;
	
	import mx.core.UIComponent;
	import mx.core.mx_internal;
	import mx.effects.Parallel;
	import mx.events.EffectEvent;
	
	import spark.components.Group;
	import spark.components.RichText;
	import spark.effects.AnimateFilter;
	import spark.effects.Move;
	import spark.effects.animation.MotionPath;
	import spark.effects.animation.SimpleMotionPath;
	import spark.filters.DropShadowFilter;
	
	public class TreeMenuNode extends UIComponent
	{
		
		static private var _sounds:Sound;
		use namespace mx_internal;
		
		/* Public Properties: ------------------------------------------------ */
		
		public var allnodeCount:uint;
		public var dispOrder:Number = 0;
		public var localDepth:Number = -1;
		
		/* Private Properties: ----------------------------------------------- */
		
		private var gridX:int = 45;
		private var gridY:int = 35;
		private var offsetY:Number = 0;
		private var _xmlnode:XMLNode;
		private var openState:Boolean = false;
		
		private var _text:String;
		
		private var cX:Array = new Array();
		private var cY:Array = new Array();
		
		private var _nodes:Array = new Array();
		private var _hlines:Array = new Array();
		private var _vline:Sprite;
		private var _icon:MovieClip;
		//private var lY;
		private var a:Number;
		private var b:Number;
		private var gosa1:Number;
		private var gosa2:Number;
		private var offset:Number;
		private var vX:Array = new Array();
		private var vY:Array = new Array();
		private var X:Array = new Array();
		private var Y:Array = new Array();
		private var hantei:Number;
		
		private var rootTreeNode:Object;
		private var lineColor:uint; 
		private var boundingLineColor:uint;
		private var boxColor:uint;
		private var textClickEffect:Parallel;
		
	
		
		/* UI Elements: ------------------------------------------------------ */
		
		/* Constructor: ------------------------------------------------------ */
		
		public function TreeMenuNode(node:XMLNode, parentNode:Object):void
		{				
				 
				rootTreeNode = parentNode;
				_xmlnode = node;
				_text = _xmlnode.firstChild.toString();
				//doubleClickEnabled = true;
				
				
				if (_sounds == null)
					_sounds = new Sound(new URLRequest("assets/sound1.mp3"));
		
				
		}
		
		
		
		/* Overriden Methods: ------------------------------------------------ *
		* ------------------------------------------------------------------- */
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			lineColor = styleParent.getStyle("lineColor");
			boundingLineColor = styleParent.getStyle("boundingLineColor");
			boxColor = styleParent.getStyle("boxColor");
			
			styleName = styleParent;

		}
		
		override protected function createChildren():void
		{
			updateDisplayList(unscaledWidth, unscaledHeight);
			
			_icon = new MovieClip();
			_icon.swell = -1;
			
			if (_xmlnode.childNodes.length == 1 && _xmlnode.lastChild.nodeType == XMLNodeType.TEXT_NODE)
			{
				//drawing a small icon
				with(_icon.graphics)
				{
					lineStyle(0, boundingLineColor);
					beginFill(boxColor);
					drawRect(0, -2.5, 5, 5);
					endFill();
				}
			}
			else if (_xmlnode.nodeType == XMLNodeType.ELEMENT_NODE)
			{
				//drawing a big icon
				with(_icon.graphics)
				{
					lineStyle(0, boundingLineColor );
					beginFill(boxColor);
					drawRect(-5, -5, 10, 10);
					endFill();
					lineStyle(0, 0xdddddd);
					//cross
					moveTo(-3.2, 0);
					lineTo(3.2,0);
					moveTo(0, -3.2);
					lineTo(0, 3.2);
				}
				_icon.buttonMode = true;
				
			}
			
			addChild(_icon);
			
			
			var group:Group = new Group();
			group.buttonMode = true;
			addChild(group);
			
			var tf:RichText = new RichText();
			tf.styleName = styleParent;
			tf.textFlow = TextConverter.importToFlow(_text, TextConverter.TEXT_FIELD_HTML_FORMAT);
			group.addElement(tf);
			group.x = 16;
			group.y = -4;
			
			
			textClickEffect = new Parallel(tf);
			textClickEffect.duration = 200;
			
			var s:Move = new Move();
			s.xFrom = tf.x;
			s.xTo = tf.x + 10;
			s.repeatBehavior = "reverse";
			s.repeatCount = 2;
			
			
			var dropShadow:AnimateFilter = new AnimateFilter(tf, new DropShadowFilter());
			dropShadow.repeatBehavior = "reverse";
			dropShadow.repeatCount = 2;
			
			var mPaths:Vector.<MotionPath> = new Vector.<MotionPath>();
			mPaths.push(new SimpleMotionPath("distance", 10, 0));
			dropShadow.motionPaths = mPaths;
			
			
			textClickEffect.addChild(s);
			textClickEffect.addChild(dropShadow);
			/*= new AnimateColor(tf);
			textClickEffect.colorFrom = tf.getStyle("color");
			textClickEffect.colorTo = getStyle("selectionColor"); 
			textClickEffect.repeatBehavior = "reverse";
			textClickEffect.repeatCount = 2;
			textClickEffect.duration = 100;*/
			
			var mask:Sprite = new Sprite();
			with(mask.graphics)
			{
				//mask to increase clicking area
				lineStyle(3,0xff0000, 1);
				beginFill(0xff0000, 1);
				drawRect(-10, -10, 20, 20);
				endFill();
			}
			mask.alpha = 0 ;
			_icon.addChild(mask);
				
			
			mask.addEventListener(MouseEvent.MOUSE_DOWN, onClick);
			mask.addEventListener(MouseEvent.MOUSE_OVER, onIconMouseOver);
			mask.addEventListener(MouseEvent.MOUSE_OUT, onIconMouseOut);
			group.addEventListener(MouseEvent.MOUSE_DOWN, onTextMouseDown);
			group.addEventListener(MouseEvent.MOUSE_UP, onTextMouseUp);
			group.addEventListener(MouseEvent.MOUSE_OVER, onTextMouseOver);
			group.addEventListener(MouseEvent.MOUSE_OUT, onTextMouseOut);
		}
		
		
		override protected function measure():void
		{
			super.measure();
			
			measuredWidth = $width;
			measuredHeight = $height;
		}
		/* Public Methods: --------------------------------------------------- *
		* ------------------------------------------------------------------- */
		public function orderPut(action:String):void
		{
			// trace("order put: " + action);
			var dispIndex:uint = 1;
			for(var i:uint = 0; i < Nodes.length; i++)
			{
				this.cX[i]  = gridX;
				this.cY[i] = gridY * dispIndex;
				Nodes[i].dispOrder = dispIndex;
				if (i == Nodes.length)
				{
					this._vline.y = offsetY;
					//this.lY = gridY * dispIndex;
				} // end if
				dispIndex = dispIndex + Nodes[i].allnodeCount;
			} // end of for
			
				
				switch (action)
				{
					case "go":
						if (parent is TreeMenuNode)
							TreeMenuNode(parent).orderPut("reflect");
						Go();
					break;
					case "close":				
						if (parent is TreeMenuNode)
							TreeMenuNode(parent).orderPut("reflect");
						//trace("send close to parent");
						removeEventListener(Event.ENTER_FRAME, Reflect);
					break;
					case "reflect":
						if (parent is TreeMenuNode)
							TreeMenuNode(parent).orderPut("reflect");
						//trace("send reflect to parent");
						addEventListener(Event.ENTER_FRAME, Reflect);
					break;
				} // end case
				
		} // End of the function
		
		
		/* Getter/Setters: --------------------------------------------------- *
		* ------------------------------------------------------------------- */
		
		public function get Nodes():Array{
			return _nodes;
		}
		
		public function get Content():String{
			return _text;
		}
		
		/* Semi-Public Methods:	---------------------------------------------- *
		* ------------------------------------------------------------------- */
		
		/* Protected Methods: ------------------------------------------------ *
		* ------------------------------------------------------------------- */		 
		
		/* Private Methods: -------------------------------------------------- *
		* ------------------------------------------------------------------- */
		
		
		private function orderChange(plusCount:uint):void
		{
			allnodeCount += plusCount;
			
			if (parent is TreeMenuNode)
			{
				TreeMenuNode(parent).orderChange(plusCount);
			}
			
		} // End of the function
		
		
		
		private function closeNode():void
		{
			var closeCount:uint;						
			_nodes.forEach(function(element:*, index:int, arr:Array):void
			{
				removeChild(element);
			});
			_nodes = new Array();
			_hlines.forEach(function(element:*, index:int, arr:Array):void
			{
				removeChild(element);
			});			
			removeChild(_vline);
			_hlines = new Array();			
			closeCount = 1 - allnodeCount;
			orderChange(closeCount);
			orderPut("close");
		} // End of the function
		
		
		
		
		private function openNode():void
		{
			 
			_vline = new Sprite();
			//_vline.blendMode = BlendMode.HARDLIGHT;
			with(_vline.graphics)
			{				
				lineStyle(0,lineColor);
				lineTo(0,100);
			}
			_vline.visible = false;
			addChild(_vline);
			swapChildren(_vline, _icon);
			
			for each(var child:XMLNode in _xmlnode.childNodes) 
			{
				if (child.nodeType == XMLNodeType.ELEMENT_NODE)
				{
					var hline:Sprite = new Sprite();
					//hline.blendMode = BlendMode.HARDLIGHT;							
					with(hline.graphics)
					{
						lineStyle(0,lineColor);
						lineTo(100,0);
					}
					addChild(hline);
					hline.visible = false;
					_hlines.push(hline);
					
					var newNode:TreeMenuNode = new TreeMenuNode(child, rootTreeNode);
					newNode.localDepth = localDepth + 1;
					newNode.allnodeCount = 1;
					addChild(newNode);
					_nodes.push(newNode);						
					
				}
			}//end cycle
			
			orderChange(Nodes.length);
			
			orderPut("go");
		}
		
		
		/*private function calcX():Number
		{
			return (gridX * localDepth);
		} 
		
		private function calcY():Number
		{
			trace("result: " + gridY * parentDispOrder(this));
			return (gridY * parentDispOrder(this));
		}
		*/
		
		
		private function parentDispOrder(node:TreeMenuNode):Number
		{
			var pNode:Object = node.parent;
			if (pNode is TreeMenuNode)
			{
				//trace ("my name: " + node.Content + " and dispOrder: " + node.dispOrder);
				return (node.dispOrder + parentDispOrder(TreeMenuNode(pNode)));
			}
			else
			{
				return node.dispOrder;
			}
			
		}
		
		private function Go():void
		{
			a = 5;
			b = 1.300000E+000;
			gosa1 = 3.000000E-002;
			gosa2 = 3.000000E-002;
			offset = offsetY;
			for(var i:uint = 0; i < Nodes.length; i++)
			{
				this.X[i] = 0;
				this.Y[i] = 0;
				this.vX[i] = 0;
				this.vY[i] = 0;
			} // end of for
			addEventListener(Event.ENTER_FRAME, Reflect);
		}
		
		
		private function Reflect(event:Event):void
		{
			
			hantei = 1;
			
			for(var i:uint = 0; i < Nodes.length; i++)
			{
				this.vX[i] = (this.vX[i] + (cX[i] - this.X[i]) / a) / b;
				this.vY[i] = (this.vY[i] + (cY[i] - this.Y[i]) / a) / b;
				this.X[i] = this.X[i] + this.vX[i];
				this.Y[i] = this.Y[i] + this.vY[i];
				if (i >= 1 && this.Y[i] < this.Y[i - 1]){
					this.Y[i] = this.Y[i - 1] * 2 - this.Y[i];
					this.vY[i] = -this.vY[i] / b;
				} // end if
				Nodes[i].x = this.X[i];
				Nodes[i].y = this.Y[i];
				_hlines[i].y = this.Y[i];
				_hlines[i].scaleX = this.X[i]/100;
				_hlines[i].visible = true;
				if (gosa1 < this.Y[i] - cY[i] || (gosa2 < this.vY[i] || this.vY[i] < -gosa2)){
					hantei = 0;
				} // end if
			} // end of for
			
			_vline.visible = true;
			
			_vline.scaleY = (this.Y[Nodes.length - 1] - offset)/100;
			
			if (hantei == 1)
			{
				//moveState = "stop";
				for(i = 0; i < Nodes.length; i++){
					this.X[i] = cX[i];
					this.Y[i] = cY[i];
					Nodes[i].x = this.X[i];
					Nodes[i].y = this.Y[i];
					_hlines[i].y = this.Y[i];
					_hlines[i].scaleX = this.X[i]/100;
				} // end of for
				_vline.scaleY = (this.Y[Nodes.length - 1] - offset)/100;
				removeEventListener(Event.ENTER_FRAME, Reflect);
			} // end if
			
			
		}
		
		
		
		
		/* Event Handlers: --------------------------------------------------- *
		* ------------------------------------------------------------------- */
		private function onClick(event:MouseEvent):void
		{
			
			if (_xmlnode.childNodes.length == 1)
				return;
			
			
			event.stopPropagation();
			
			//trace("child length: " + _xmlnode.childNodes.length);
			
			_sounds.play();
			if (!openState)
			{
				//_rootTreeNode.setShiftPoint(-calcX(), -calcY());
				
				openNode();
				
				with(_icon.graphics)
				{
					clear();
					lineStyle(0, boundingLineColor);
					beginFill(boxColor);
					drawRect(-5, -5, 10, 10);
					endFill();
					lineStyle(0, 0xdddddd);
					moveTo(-3.2, 0);
					lineTo(3.2,0);
				}
			}
			else
			{							
				//_rootTreeNode.setShiftPoint(-calcX(), -calcY());
				
				closeNode();
				
				with(_icon.graphics){
					clear();
					lineStyle(0, boundingLineColor);
					beginFill(boxColor);
					drawRect(-5, -5, 10, 10);
					endFill();
					lineStyle(0, 0xdddddd);
					//cross
					moveTo(-3.2, 0);
					lineTo(3.2,0);
					moveTo(0, -3.2);
					lineTo(0, 3.2);
				}
			}//endelse
			
			openState = !openState;
			
			//rootTreeNode.setDeltaPoint();
			
			rootTreeNode.invalidateDynamics();
			
		}//endfunction
		
		
		private function onIconSwell(event:Event):void
		{
			if (_icon.swell >= _icon.maxLim)
			{
				_icon.removeEventListener(Event.ENTER_FRAME, onIconSwell);
				
				if (_icon.swell >= 1)
					_icon.swell = -1;
				
			}
			
			_icon.scaleY = _icon.scaleX = 1.5 - Math.pow(_icon.swell,2)/2;
			_icon.swell += 0.1;
		}
		
		private function onIconMouseOver(event:MouseEvent):void{
			_icon.maxLim = 0.75;
			_icon.addEventListener(Event.ENTER_FRAME, onIconSwell);
			
		}
		
		private function onIconMouseOut(event:MouseEvent):void{
			_icon.maxLim = 1;
			_icon.addEventListener(Event.ENTER_FRAME, onIconSwell);
		}
		
		private function onTextMouseOver(event:MouseEvent):void
		{
			_icon.maxLim = 0.75;
			_icon.addEventListener(Event.ENTER_FRAME, onIconSwell);			
		}
		
		private function onTextMouseOut(event:MouseEvent):void
		{
			_icon.maxLim = 1;
			_icon.addEventListener(Event.ENTER_FRAME, onIconSwell);			
		}
	
		
		private function onTextMouseDown(event:MouseEvent):void
		{
				rootTreeNode.isPendingTextClick = true;
			
		}
		
		private function onTextMouseUp(event:MouseEvent):void
		{
			if (rootTreeNode.isPendingTextClick)
			{
				textClickEffect.play();
				var f:Function = function(event:EffectEvent):void{
					dispatchEvent(new TreeMenuEvent(TreeMenuEvent.SELECTED, XML(_xmlnode)));
					textClickEffect.removeEventListener(EffectEvent.EFFECT_END, f);
				}
				textClickEffect.addEventListener(EffectEvent.EFFECT_END, f );
			}
		}
		

	}
}